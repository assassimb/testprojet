#ifndef BALLON_OU_MUR_H
#define BALLON_OU_MUR_H

#include <LibRobus.h>
#include <math.h>


double distancecapteurbas ();
double distancecapteurhaut ();
int ballonoumur();
int trouverballon ();
int trouver_ballon_ligne();
int un_ou_zero();
double moyenne_distancebas();
int reach_ballon();

#endif