/**
 * \file   fonctions_capteurs.h
 * \author Team Sac-A-Bot
 * \date   22 octobre 2019
 * \brief  Entète du fichier fonctions_capteurs.cpp
 * \version 1.0
 */

#ifndef FONCTIONSCAPTEURS_H
#define FONCTIONSCAPTEURS_H

#include <LibRobus.h>
#include <math.h>

double lire_capteur_distance(int id);

#endif