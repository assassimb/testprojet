#include "fonctions_capteurs.h"

double lire_capteur_distance(int id)
{
    unsigned int valBytes = ROBUS_ReadIR(id); //Lit le capteur et retourne une valeur entre 0 et 1023 (10 bits)
    //Serial.print("valBytes: "); Serial.println(valBytes);
    double valVolts = ((double)valBytes * 5) / 1023; //Conversion numérique vers tension
    //Serial.print("valVolts: "); Serial.println(valVolts);
    double distance = 27.726 * pow(valVolts, -1.2045); //Voir https://www.makerguides.com/sharp-gp2y0a21yk0f-ir-distance-sensor-arduino-tutorial/

    if (valVolts < 0.4 || valVolts > 2.5) //Si on dépasse les limites de mesure du capteur...
    {
        Serial.print("Capteur optique ");
        Serial.print(id);
        Serial.println("Zone indéfinie");
        return -1;
    }

    else
    {
        Serial.print("Capteur optique ");
        Serial.print(id);
        Serial.print("+*9: ");
        Serial.print(distance);
        Serial.println("cm\n");
        return distance;
    }
}