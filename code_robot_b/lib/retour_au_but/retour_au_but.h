#ifndef RETOURAUBUT_H
#define RETOURAUBUT_H

#include <LibRobus.h>
#include <math.h>

enum Couleur
{
    Jaune,
    Vert,
    Bleu,
    Rouge
};

void retour_au_but(Couleur c);

#endif