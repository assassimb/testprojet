/**
* \file   retour_au_but.cpp
* \author Team Sac-A-Bot
* \date   06 novembre 2019
* \brief  Librairie des fonctions permettant de se retourner vers les buts de couleurs différentes
* \version 1.0
*/

#include "retour_au_but.h"
#include "fonctions_moteurs.h"

void retour_au_but(Couleur c)
{
    ligne_droite(10); //
    switch(c)
    {
        case Jaune:
        tourner_droite(45);
        ligne_droite(40);
            break;

        case Vert:
        tourner_gauche(125);
        ligne_droite(45);
            break;

        case Bleu:
        tourner_gauche(45);
        ligne_droite(40);
            break;

        case Rouge:
        tourner_droite(125);
        ligne_droite(45);
            break;
    }
}