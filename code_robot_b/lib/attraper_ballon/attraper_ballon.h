#ifndef ATTRAPER_BALLON_H
#define ATTRAPER_BALLON_H

#include <LibRobus.h>

void setup_servo();
void attrape_ballon();
void relacher_ballon();
int servo_move(int deg);

#endif