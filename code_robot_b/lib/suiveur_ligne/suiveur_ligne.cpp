/**
 * \file   suiveur_ligne.cpp
 * \author Team Sac-A-Bot
 * \date   22 octobre 2019
 * \brief  Librairie des fonctions permettant au robot de suivre une ligne
 * \version 1.0
 */

#include "suiveur_ligne.h" /**Importe la librairie des fonctions qui permettent
                                de suivre des lignes */
#include "PID.h"
#include "fonctions_capteurs.h"
#include <LibRobus.h>
#include "fonctions_moteurs.h"

#define ERREUR 30
#define CAPTEUR_1 2.87109375
#define CAPTEUR_2 1.440429688
#define CAPTEUR_3 0.666

/**
*\brief Permet au robot de suivre une ligne.
*
*Ajuste les moteurs pour permettre au robot de suivre la ligne selon
*   les valeurs des capteurs du suiveur de ligne. Tant que le robot
*   ne croise pas de ligne, la fonction ne fait rien.
*
*/
void suivre_ligne()
{
    if (est_parallele() == 1)
    {   
        Serial.println("PARRALELLE");
        reset_encoders();
        delay(20);
        if(ENCODER_Read(1) < 5)
        {
            acceleration(0);
        }
        avancer();

    } else if (est_perpendiculaire() == 1)
    {
        Serial.println("PERPENDICULAIRE");
        deceleration();
        tourner_gauche_luimeme(90);
    } else if (est_cote_droit())
    {
        Serial.println("DROITE");
        oriente_gauche();
    } else if (est_cote_gauche())
    {
        Serial.println("GAUCHE");
        oriente_droite();
    }
}

/**
*\brief Permet au robot de dévier légèrement vers la gauche
*
*Ajuste les moteurs pour permettre au robot de dévier lentement vers la gauche
*
*
*/
void oriente_gauche()
{
    MOTOR_SetSpeed(0, 0.3);
    MOTOR_SetSpeed(1, 0.34);
}

/**
*\brief Permet au robot de dévier légèrement vers la droite
*
*Ajuste les moteurs pour permettre au robot de dévier lentement vers la droite
*
*
*/
void oriente_droite()
{
    MOTOR_SetSpeed(0, 0.34);
    MOTOR_SetSpeed(1, 0.3);
}


/**
*\brief Détecte si le robot est perpendiculaire à une ligne.
*
*   Si aucun capteur n'est activé, alors le robot est perpendiculaire
*       à une ligne.
*
*\return Retourne 1 si le robot est perpendiculaire à une ligne, retourne 0
*   si le robot n'est pas perpendiculaire à une ligne.
*/
int est_perpendiculaire()
{
    if (capteurs_actives() == 0)
    {
        return 1;
    } else {
        return 0;
    }
    return 0;
}

/**
*\brief Détecte si le robot est parallèle à une ligne.
*
*Si les capteurs 1 et 3 sont activés, alors le robot est parallèle
*   à une ligne.
*
*\return Retourne 1 si le robot est parallèle à une ligne, retourne 0
*   si le robot n'est pas parallèle à une ligne.
*/
int est_parallele()
{
    if (capteurs_actives() == 13)
    {
        return 1;
    } else {
        return 0;
    }
    return 0;
}

int sont_tous_actives()
{
    if(capteurs_actives() == 123)
    {
        return 1;
    } else
    {
        return 0;
    }
    
}

/**
*\brief Détecte si le robot tend à avoir la ligne sur son côté gauche.
*
*\return Retourne 1 si le robot a la ligne à sa gauche, retourne 0
*   si le robot n'a pas la ligne à sa gauche.
*/
int est_cote_gauche()
{
    if (capteurs_actives() == 23 || capteurs_actives() == 3)
    {
        return 1;
    } else {
        return 0;
    }
    return 0;
}

/**
*\brief Détecte si le robot tend à avoir la ligne sur son côté droit.
*
*\return Retourne 1 si le robot a la ligne à sa droite, retourne 0
*   si le robot n'a pas la ligne à sa droite.
*/
int est_cote_droit()
{
    if (capteurs_actives() == 12 || capteurs_actives() == 1)
    {
        return 1;
    } else {
        return 0;
    }
    return 0;
}


/**
*\brief Détecte quel(s) capteur(s) sont/est activé(s).
*
*
*\return Retourne un entier qui détermine quel capteur est activé.
*   1: Capteur 1 (droite) activé
*   2: Capteur 2 (centre) activé
*   3: Capteur 3 (gauche) activé
*   12: Capteurs 1 et 2 activés
*   13: Capteurs 1 et 3 activés
*   23: Capteurs 2 et 3 activés
*   123: Capteurs 1, 2 et 3 activés
*   0: Aucun capteur activé
*/
/*
int capteurs_actives()
{
    if (((CAPTEUR_1 / 5) * 1024) - ERREUR < lire_suiveur_ligne() && 
            lire_suiveur_ligne() < ((CAPTEUR_1 / 5) * 1024) + ERREUR)
    {
        // Serial.println("Capteur 1 activé");
        return 1;
    } else if (((CAPTEUR_2 / 5) * 1024) - ERREUR < lire_suiveur_ligne() && 
            lire_suiveur_ligne() < ((CAPTEUR_2 / 5) * 1024) + ERREUR)
    {
        // Serial.println("Capteur 2 activé");
        return 2;
    } else if (((CAPTEUR_3 / 5) * 1024) - ERREUR < lire_suiveur_ligne() && 
            lire_suiveur_ligne() < ((CAPTEUR_3 / 5) * 1024) + ERREUR) 
    {
        // Serial.println("Capteur 3 activé");
        return 3;
    } else if ((((CAPTEUR_1 + CAPTEUR_2) / 5) * 1024) - ERREUR < 
            lire_suiveur_ligne() && lire_suiveur_ligne() < 
            (((CAPTEUR_1 + CAPTEUR_2) / 5) * 1024) + ERREUR) 
    {
        // Serial.println("Capteur 1 et 2 activé");
        return 12;
    } else if ((((CAPTEUR_1 + CAPTEUR_3) / 5) * 1024) - ERREUR < 
            lire_suiveur_ligne() && lire_suiveur_ligne() < 
            (((CAPTEUR_1 + CAPTEUR_3) / 5) * 1024) + ERREUR) 
    {
        // Serial.println("Capteur 1 et 3 activé");
        return 13;
    } else if ((((CAPTEUR_2 + CAPTEUR_3) / 5) * 1024) - ERREUR < 
            lire_suiveur_ligne() && lire_suiveur_ligne() < 
            (((CAPTEUR_2 + CAPTEUR_3) / 5) * 1024) + ERREUR) 
    {
        // Serial.println("Capteur 2 et 3 activé");
        return 23;
    } else if ((((CAPTEUR_1 + CAPTEUR_2 + CAPTEUR_3) / 5) * 1024) - ERREUR < 
            lire_suiveur_ligne() && lire_suiveur_ligne() < 
            (((CAPTEUR_1 + CAPTEUR_2 + CAPTEUR_3) / 5) * 1024) + ERREUR) 
    {
        // Serial.println("3 capteurs activés");
        return 123;
    } else 
    {
        return 0;
    }
    return 0;
}
*/