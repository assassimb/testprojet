/**
 * \file   direction_vers_couleur.h
 * \author Team Sac-A-Bot
 * \date   04 novembre 2019
 * \brief  Entète du fichier direction_vers_couleur.cpp
 * \version 1.0
 */

#ifndef DIRECTION_VERS_COULEUR_H
#define DIRECTION_VERS_COULEUR_H

#include <LibRobus.h>
#include <math.h>

void bleu();
void jaune();
void vert();
void orange();


#endif