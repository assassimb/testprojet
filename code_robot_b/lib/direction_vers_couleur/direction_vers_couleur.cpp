/**
* \file   direction_vers_couleur.cpp
* \author Team Sac-A-Bot
* \date   04 novembre 2019
* \brief  Librairie des fonctions permettant de se déplacer vers les couleurs
* \version 1.0
*/

#include "direction_vers_couleur.h"
#include <fonctions_moteurs.h>
#include <suiveur_ligne.h>

void bleu(){
tourner_droite_luimeme(45);
while(!est_perpendiculaire())
{

MOTOR_SetSpeed(0,0.25);
MOTOR_SetSpeed(1,0.25);
}
ligne_droite(1);
tourner_droite_luimeme(90);
suivre_ligne();
}

void jaune(){

    tourner_gauche_luimeme(45);
    while(est_perpendiculaire() == 0)
    {
        MOTOR_SetSpeed(0,0.25);
        MOTOR_SetSpeed(1,0.25);
    }
ligne_droite(1);
tourner_gauche_luimeme(90);

while(sont_tous_actives() == 0)
{
    suivre_ligne();
}

}

void vert(){

tourner_droite_luimeme(90);
ligne_droite(35);
tourner_gauche_luimeme(90);
ligne_droite(60);
tourner_gauche_luimeme(45);
while(est_perpendiculaire() == 0)
{

MOTOR_SetSpeed(0,0.25);
MOTOR_SetSpeed(1,0.25);
}
ligne_droite(1);
tourner_droite_luimeme(90);
suivre_ligne();

}

void orange(){
tourner_gauche_luimeme(90);
ligne_droite(35);
tourner_droite_luimeme(90);
ligne_droite(60);
tourner_droite_luimeme(45);
while(!est_perpendiculaire())
{

MOTOR_SetSpeed(0,0.25);
MOTOR_SetSpeed(1,0.25);
}
ligne_droite(1);
tourner_gauche_luimeme(90);
suivre_ligne();


}