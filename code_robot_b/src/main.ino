/*
Projet: Le nom du script
Equipe: Votre numero d'equipe
Auteurs: Les membres auteurs du script
Description: Breve description du script
Date: Derniere date de modification
*/

/* ****************************************************************************
Inclure les librairies de functions que vous voulez utiliser
**************************************************************************** */
#include <LibRobus.h> // Essentielle pour utiliser RobUS
#include <math.h>	 //Importe les fonctions de mathématique
#include "PID.h"	  // les "" indique que le fichier est local et <> est a standart location
#include "fonctions_moteurs.h"
#include "fonctions_capteurs.h"
#include "attraper_ballon.h"
#include "direction_vers_couleur.h"
#include "ballon_ou_mur.h"
#include "retour_au_but.h"
#include <avr/io.h>
#include <avr/interrupt.h>

void init_servo();
void bouger_servo(int angle);
void init_couleur();
String analyse_couleur();
void lecture_suiveur_ligne();
void activation_bumper();

int compteurTimer = 3;	//Offset pour compenser

void change_LED_state()
{
	char val = digitalRead(LED_BUILTIN);
	digitalWrite(LED_BUILTIN, !val);
	compteurTimer++;
}

void avance_milieu()
{
	ligne_droite(10);
}

/* **********************************0******************************************
Variables globales et defines
**************************************************************************** */

/* ****************************************************************************
Fonctions d'initialisation (setup)
**************************************************************************** */
void setup()
{
	BoardInit();
	// init_couleur();
	//init_servo();
	//init_couleur();
	//init_timer_interrupt();
	SOFT_TIMER_SetCallback(0, change_LED_state);
	SOFT_TIMER_SetDelay(0, 500);
	SOFT_TIMER_SetRepetition(0, 120);

	pinMode(LED_BUILTIN, OUTPUT);
	digitalWrite(LED_BUILTIN, 0);

	pinMode(A1, INPUT);

	//Set la pince
	attrape_ballon();
}

/* ****************************************************************************
Fonctions de boucle infini (loop())
**************************************************************************** */
void loop()
{
	/*
  bouger_servo(90);
  activation_bumper();
  while ((abs(lire_capteur_distance(1) - lire_capteur_distance(2)) > 12) && (lire_capteur_distance(2) > double(15)))
  {
    ligne_droite(1);
    lire_capteur_distance(2);
    lire_capteur_distance(1);
  }
  bouger_servo(0);
  tourner_droite(45);
  tourner_droite(10);
  */

	while(!ROBUS_IsBumper(3)){
        // Do nothing
    }

	// SOFT_TIMER_Enable(0);

	// //Attend une minute
	// while(compteurTimer < 120)
	// {
	// 	SOFT_TIMER_Update();
	// 	delay(1);
	// }
	relacher_ballon(); //mets la pince en mode active afin de ne pas toucher au robot A

	digitalWrite(LED_BUILTIN, 1); 	//Allume la DEL

	avance_milieu();	//S'avance vers le milieu

	trouver_ballon_ligne(); //Cherche le ballon au milieu

	attrape_ballon(); //Attrape le ballon

	retour_au_but(Couleur::Jaune);	//Se dirige vers le but d'une couleur.

	relacher_ballon(); //Relache le ballon

	while(1);	//S'arrete (boucle infinie)
}
