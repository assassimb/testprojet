/*
Projet: Le nom du script
Equipe: Votre numero d'equipe
Auteurs: Les membres auteurs du script
Description: Breve description du script
Date: Derniere date de modification
*/

/* ****************************************************************************
Inclure les librairies de functions que vous voulez utiliser
**************************************************************************** */
#include <LibRobus.h> // Essentielle pour utiliser RobUS
#include "fonctions_moteurs.h"

/* ****************************************************************************
Variables globales et defines
**************************************************************************** */
#define pinLigne A1
float valOutput[] = //ne pas oublier que les lectures sont inversé car les linges sont noir
{
  0.00, //aucun capteur **perdu dans le vide*
  2.86, //v1  **tourner droite -**
  1.44, //v2  **impossible**
  4.30, //v1 et v2  **tourner droite+**
  0.74, //v3  **tourner gauche-**
  3.60, //v1 et v3  **tout droit**
  2.18, //v2 et v3  **tourner gauche+**
  4.96  //tout en meme temps **active le capteur de dist**
};

/* ****************************************************************************
Lecture du suiveur de ligne
**************************************************************************** */
void lecture_suiveur_ligne(){
  float incertitudeLecture = 0.2;
  float lecture = ((5.0f/1024.0f))*(double)analogRead(A1);//faire le calcul ici
  Serial.print(lecture);
  if (lecture > (valOutput[0]- incertitudeLecture) && lecture < (valOutput[0]+ incertitudeLecture) )
  {
    //accelere une roue
    Serial.println("aucun capteur");
  }
  else if (lecture > (valOutput[1]- incertitudeLecture) && lecture < (valOutput[1]+ incertitudeLecture) )
  {
    tourner_droite(1); //faudrait faire dequoi pour que le robot arrete pas
    //accelere une roue
    Serial.println("v1");
  }
  else if (lecture > (valOutput[2]- incertitudeLecture) && lecture < (valOutput[2]+ incertitudeLecture) )
  {
    //accelere une roue**impossible**
    Serial.println("v2");
  }
  else if (lecture > (valOutput[3]- incertitudeLecture) && lecture < (valOutput[3]+ incertitudeLecture) )
  {
    tourner_gauche(3);
    //accelere une roue
    Serial.println("v1 et v2");
  }
  else if (lecture > (valOutput[4]- incertitudeLecture) && lecture < (valOutput[4]+ incertitudeLecture) )
  {
    tourner_gauche(1);
    //accelere une roue
    Serial.println("v3");
  }
  else if (lecture > (valOutput[5]- incertitudeLecture) && lecture < (valOutput[5]+ incertitudeLecture) )
  {
    ligne_droite(1);  //verifier la distance!!
    //accelere une roue
    Serial.println("v1 et v3");
  }
  else if (lecture > (valOutput[6]- incertitudeLecture) && lecture < (valOutput[6]+ incertitudeLecture) )
  {
    tourner_droite(3);
    //accelere une roue
    Serial.println("v2 et v3");
  }
  else if (lecture > (valOutput[7]- incertitudeLecture) && lecture < (valOutput[7]+ incertitudeLecture) )
  {
    ligne_droite(0);  //verifier la distance!! pour arreter
    //accelere une roue
    Serial.println("tout en meme temps");
  }
}