/*
Projet: Le nom du script
Equipe: Votre numero d'equipe
Auteurs: Les membres auteurs du script
Description: Breve description du script
Date: Derniere date de modification
*/

/* ****************************************************************************
Inclure les librairies de functions que vous voulez utiliser
**************************************************************************** */

#include <LibRobus.h> // Essentielle pour utiliser LibRobus
#include <Wire.h>
#include "Adafruit_TCS34725.h"

//définition des variables globales
#define commonAnode true

// our RGB -> eye-recognized gamma color
byte gammatable[256];
Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_4X);


/* ****************************************************************************
initialisation du capteur de couleur
**************************************************************************** */
void init_couleur(){
  //vérification de la connexion au capteur
  if (tcs.begin()) {
    Serial.println("Found sensor");
  } else {
    Serial.println("No TCS34725 found ... check your connections");
    while (1); // halt!
  }
  //création de la table de conversion
  //en RGB it helps convert RGB colors to what humans see
  for (int i=0; i<256; i++) {
    float x = i;
    x /= 255;
    x = pow(x, 2.5);
    x *= 255;

    if (commonAnode) {
      gammatable[i] = 255 - x;
    } else {
      gammatable[i] = x;
    }
  }
}

/* ****************************************************************************
Analyse des couleurs
**************************************************************************** */
String analyse_couleur(){
  float red, green, blue;
  String couleur= "\0";
  int minimum = 65; //valeur minimum de detection selon l'éclairage
  int seuil_detection = 65;  //valeur ou la détection commence

    tcs.setInterrupt(false);  // turn on LED
    delay(60);  // takes 50ms to read
    tcs.getRGB(&red, &green, &blue);
    tcs.setInterrupt(true);  // turn off LED

    if (red < minimum && green < minimum && blue < minimum)
    {
      couleur = "noir";
    }
    else if (red > seuil_detection && green > seuil_detection && blue > seuil_detection)
    {
      couleur = "blanc";
    }
    else if (red > seuil_detection && green < minimum && blue < minimum)
    {
      couleur = "rouge";
    }
    else if (red < minimum && green < minimum && blue > seuil_detection)
    {
      couleur = "bleu";
    }
    else if (red < minimum && green > seuil_detection && blue < minimum)
    {
      couleur = "vert";
    }
    else if (red > seuil_detection && green > seuil_detection && blue < minimum)
    {
      couleur = "jaune";
    }
    else if (red > seuil_detection && green < minimum && blue > seuil_detection)
    {
      couleur = "magenta";
    }
    else if (red < minimum && green > seuil_detection && blue > seuil_detection)
    {
      couleur = "cyan";
    }
    else
    {
      couleur = "pas dans les possibilitées";
    }

    Serial.print("R:\t"); Serial.print(int(red)); 
    Serial.print("\tG:\t"); Serial.print(int(green)); 
    Serial.print("\tB:\t"); Serial.print(int(blue));
    Serial.print("\n");
    Serial.println(analyse_couleur());
    return couleur;
}
  /*analogWrite(redpin, gammatable[(int)red]);
  analogWrite(greenpin, gammatable[(int)green]);
  analogWrite(bluepin, gammatable[(int)blue]);*/