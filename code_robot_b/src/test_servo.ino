/*
Projet: Le nom du script
Equipe: Votre numero d'equipe
Auteurs: Les membres auteurs du script
Description:contient --> init_servo(); , bouger_servo(int angle)
Date: Derniere date de modification
*/

/* ****************************************************************************
Inclure les librairies de functions que vous voulez utiliser
**************************************************************************** */
#include <LibRobus.h> // Essentielle pour utiliser RobUS

/* ****************************************************************************
Variables globales et defines
**************************************************************************** */
void init_servo();
void bouger_servo(int angle);

/* ****************************************************************************
initialisation du Servo
**************************************************************************** */
void init_servo(){
  SERVO_Enable(0);
  SERVO_SetAngle(0, 0);
  delay(1000);
}

/* ****************************************************************************
mouvement du servo
**************************************************************************** */
void bouger_servo(int angle) {
  SERVO_SetAngle(0, angle);
  delay(1000);//voir s'il faut un millis()
}